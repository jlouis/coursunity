﻿Shader "Hidden/NewImageEffectShader"
{
	Properties
	{
		_Input ("Input", Range (0.00,1.00)) = 1.00 // sliders
		_Palier ("Palier", Range (0,50)) = 1 // sliders
		
		
		_TimeE ("Time", Range (0,100)) = 1 // sliders
		
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float _Input;
			int _Palier;
			float _TimeE;

			fixed4 frag (v2f i) : SV_Target
			{
				float r = abs(sin(_TimeE * 2) + 0.1);
				float g = abs(sin (_TimeE) + 0.1);
				float b = abs(sin (_TimeE * 1.5) + 0.1);
				
				
				float deformFact = sin(_TimeE);
				deformFact = deformFact * 0.1;
				//if (deformFact < 0.5f)
				//	deformFact = 0.5f;
					
					//if (deformFact > 0.5f)
					//deformFact = 0.5;
					
				i.uv.x = i.uv.x + i.uv.y *sin(i.uv.y * _TimeE * 4) / 10.0;
				
				
				
			
				fixed4 col = tex2D(_MainTex, i.uv);
				

				// just invert the colors
				float intensity = (col.r + col.g + col.b) / 3;
				
				float palier = 1.0 / _Palier;
				float palierCumul = 0.0;
				
				while (palierCumul < intensity)
				{
					palierCumul = palierCumul + palier;
				}
				
				intensity = palierCumul;
					
				
				col.r = col.r *  intensity;
				col.g = col.g * intensity;
				col.b = col.b * intensity;
				
				col = col * _Input;
				
				float moyenne = (col.r + col.g + col.b) / 3;
				//col.r = moyenne * r;
				//col.g = moyenne * g;
				//col.b = moyenne * b;

				return  col;
			}
			ENDCG
		}
	}
}
