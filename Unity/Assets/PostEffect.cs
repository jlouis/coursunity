﻿using UnityEngine;
using System.Collections;

public class PostEffect : MonoBehaviour {

    public Material mat;

    private float time;
    public float animSpeed = 1.0F;

	// Use this for initialization
	void Start () {
        time = 0;
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime * animSpeed;
	}

    // Called by the camera to apply the image effect
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        mat.SetFloat("_TimeE", time);

        //mat is the material containing your shader
        Graphics.Blit(source, destination, mat);
    }
}
