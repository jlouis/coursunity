﻿using UnityEngine;
using System.Collections;

public class CamCinematics : MonoBehaviour {

    [System.Serializable]
    public class Point
    {
        public Transform point;
        public float speed = 1.0f;
    }

	// série d'empty gameobject pour placer les différentes positions de la cam
	public Point[] points;

	public float globalSpeed = 1.0f;
	public bool lookTargetPoint;
	public Transform targetPoint;
    public bool smoothTarget;

	private int index = 0;
	private float timer = 0.0f;

	private Camera cam;

	// Use this for initialization
	void Start () {
		// Si le script est actif on met cette cam en cam principale
		cam = GetComponent<Camera>();
        // TODO : camera switch
        cam.enabled = true;
        Camera.main.enabled = false;
	}

	int getNextIndex(int index)
	{
		return (index + 1) % points.Length;
	}
	
	// Update is called once per frame
	void Update () {

		if (points.Length == 0 || points.Length == 1)
			return;

		int nextIndex = getNextIndex(index);
		Vector3 startPos = points[index].point.position;
		Vector3 endPos = points[nextIndex].point.position;

		timer += Time.deltaTime * globalSpeed * points[index].speed;

		Vector3 camPos = Vector3.Slerp(startPos, endPos, timer);
		transform.position = camPos;

		if (lookTargetPoint) {
            if (!smoothTarget)
			    transform.LookAt (targetPoint.position);
            else
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetPoint.position - transform.position), Time.deltaTime);
        }

		if (timer >= 1.0f) {
			index = getNextIndex (index);
			timer = 0;
		}
	}
}
