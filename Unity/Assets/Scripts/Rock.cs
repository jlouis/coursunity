﻿using UnityEngine;
using System.Collections;

public class Rock : MonoBehaviour {

	public MoutonController sheepScript;

	void OnCollisionEnter(Collision coll)
	{
		if (coll.gameObject == sheepScript.gameObject) {
			// TODO : Fail
			sheepScript.Kill();
		}
	}
}
