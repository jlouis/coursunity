﻿using UnityEngine;
using System.Collections;

public class MoutonController : MonoBehaviour {


	public float speed = 1.0f;
    public float jumpHeight = 1.0f;

    Rigidbody rb;
    Animator anim;

    bool isFalling = false;

	bool killed = false;

	public void Kill()
	{
		killed = true;
		transform.localScale = new Vector3(transform.localScale.x, 0.1f , transform.localScale.z);
		rb.isKinematic = true;
	}

    void Jump()
    {
        if (isFalling)
            return;

        Vector3 newVelocity = rb.velocity;
        newVelocity.y = jumpHeight;
        rb.velocity = newVelocity;
        isFalling = true;
        anim.SetBool("jump", true);
    }


    void MoveTo(Vector3 movement)
    {
        float velY = rb.velocity.y;
        Vector3 newVelocity = movement * speed;
        newVelocity.y = velY;
        rb.velocity = newVelocity;
    }

    void OnCollisionStay(Collision coll)
    {
        if (rb.velocity.y > 0)
            return;

        //we are on something
        isFalling = false;
        anim.SetBool("jump", false);
    }


    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update ()
	{
		if (killed) {
			anim.SetBool ("jump", false);
			anim.SetFloat ("speed", 0);
			MoveTo (Vector3.zero);
			return;
		}

		Vector3 movement = Vector3.zero;
        if (Input.GetKey (KeyCode.Z))
			movement.z += 1.0f;
		if (Input.GetKey (KeyCode.S))
			movement.z -= 1.0f;
		if (Input.GetKey (KeyCode.Q))
			movement.x -= 1.0f;
		if (Input.GetKey (KeyCode.D))
			movement.x += 1.0f;

        /*
        Quaternion rotMatrix = new Quaternion();
        rotMatrix.SetEulerRotation(transform.rotation.ToEuler());
        movement = rotMatrix * movement;*/

		if (movement != Vector3.zero)
		{
			movement.Normalize ();
			movement *= speed;

			float angle = Mathf.Atan2 (movement.x, movement.z);
			angle *= Mathf.Rad2Deg;
			transform.rotation = Quaternion.Euler (0, angle, 0);


		}

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }

		MoveTo(movement);
        anim.SetFloat("speed", rb.velocity.magnitude);
	}
}
