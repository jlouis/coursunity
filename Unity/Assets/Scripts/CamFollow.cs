﻿using UnityEngine;
using System.Collections;

public class CamFollow : MonoBehaviour {

    public Transform target;

    private Vector3 diff;

	// Use this for initialization
	void Start () {
        diff = target.position - transform.position;

    }
	
	// Update is called once per frame
	void Update () {
        transform.position = target.position - diff;
	}
}
