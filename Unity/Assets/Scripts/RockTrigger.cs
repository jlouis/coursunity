﻿using UnityEngine;
using System.Collections;

public class RockTrigger : MonoBehaviour {

	public Rigidbody rbRock;

	void OnTriggerEnter()
	{
		rbRock.isKinematic = false;
	}
}
